import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-multi-form',
  templateUrl: './multi-form.component.html',
  styleUrls: ['./multi-form.component.css']
})
export class MultiFormComponent implements OnInit {
  public data: any[] = new Array();
  public lastData: any[] = new Array();
  public completed: boolean;
  public progress: number;
  public steps: number;
  public stepLength: number;
  public currentStep: number;

  constructor() {
    this.currentStep = 1;
    this.steps = 3;
    this.stepLength = 100/this.steps;
    this.progress = this.stepLength * this.currentStep;
    this.completed = false;
  }

  ngOnInit() {
  }

  nextStep(form): void {
    if(form) {
      this.setData(form);
      this.renderLastData();
      this.currentStep++;
      this.getProgress();
      this.isCompleted();
    }
  }

  prevStep(isBack: boolean): void {
    this.currentStep--;
    this.getProgress();
  }

  setData(data: any[]): void {
    this.data.splice(this.currentStep - 1, 1, data);
  }

  renderLastData(): void {
    this.lastData = this.data[this.currentStep - 1];
  }

  getProgress() {
    this.progress = this.stepLength * this.currentStep;
  }

  isCompleted(): void {
    if(this.currentStep === this.steps) {
      this.completed = true;
    }
  }
}
