import { FormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MultiFormComponent } from './multi-form.component';

describe('MultiFormComponent', () => {
  let component: MultiFormComponent;
  let fixture: ComponentFixture<MultiFormComponent>;
  let minSteps: number = 2;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [  MultiFormComponent ],
      imports: [ FormsModule ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should more than ${minSteps} (min steps)`, () => {
    expect(component.steps).not.toBeLessThan(minSteps);
  });

  it(`should be two (next step)`, () => {
    let form: boolean = true;
    component.currentStep = 1;

    component.nextStep(form);
    expect(component.currentStep).toBe(2);
  });

  it(`should be 1 (prev step)`, () => {
    let form: boolean = true;
    let isBack: boolean = true;
    component.currentStep = 2;

    component.prevStep(isBack);
    expect(component.currentStep).toBe(1);
  });

  it(`should be 50 percents (progress)`, () => {
    let form: boolean = true;
    component.currentStep = 1;
    component.stepLength = 25;

    component.getProgress();
    component.nextStep(form);
    expect(component.progress).toBe(50);
  });

  it(`should be 25 percents (progress)`, () => {
    let form: boolean = true;
    let isBack: boolean = true;
    component.currentStep = 2;
    component.stepLength = 25;

    component.prevStep(isBack);
    component.getProgress();
    expect(component.progress).toBe(25);
  });

  it(`should be true (completed)`, () => {
    let form: boolean = true;
    component.currentStep = 3;
    component.steps = 4;

    component.nextStep(form);
    component.isCompleted();
    expect(component.completed).toBe(true);
  });

  it(`should be previus obj from array data`, () => {
    let isBack: boolean = true;
    let newData =
    {
      'login': 'petya_4ornuy',
      'email': 'petya_likes_horses@gmail.com',
      'password': '4avella'
    };

    component.data = [
      {
        'login': 'burbon',
        'email': 'burbon91@gmail.com',
        'password': 'password'
      }
    ];

    component.currentStep = 1;
    component.nextStep(newData);
    component.prevStep(isBack);
    component.nextStep(newData);

    expect(component.data[component.currentStep - 2]).toBe(newData);
  });

});
