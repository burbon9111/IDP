import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toArray'
})
export class ToArrayPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let data: any[] = new Array();

    for(let key in value) {
      data.push({key: key, value: value[key]});
    }
    return data;
  }

}
