import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appPhoneMask]'
})
export class PhoneMaskDirective implements OnInit{
  constructor(private el: ElementRef) {
  }

  ngOnInit() {
  }

  @HostListener('input', ['$event'])
  inputChanged(event) {
    let pos: number = event.target.value
      .replace(/\D/g, '')
      .match(/(\d{0,3})(\d{0,3})(\d{0,4})/);

    event.target.value = !pos[2] ? pos[1] : + pos[1] + '-' + pos[2] + (pos[3] ? '-' + pos[3] : '');
  }
}
