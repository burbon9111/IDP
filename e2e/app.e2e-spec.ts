import { IDPPage } from './app.po';

describe('idp App', () => {
  let page: IDPPage;

  beforeEach(() => {
    page = new IDPPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
